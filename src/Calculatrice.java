import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

public class Calculatrice implements ActionListener {
	
	private JFrame frame; //Fen�tre
	private JTextField textfield; //Ecran de la calculatrice
	
	//Instanciation des boutons
	private JButton[] nombres = new JButton[10]; //Bouton chiffre de la calculatrice (10 chiffres)
	private JButton[] operations = new JButton[9]; // Bouton op�rations de la calculatrice (9 op�rations)
	private JPanel panel;
	
	private JButton btPlus; //Bouton +
	private JButton btSous; //Bouton -
	private JButton btMult; //Bouton *
	private JButton btDiv; //Bouton /
	private JButton btNega; //Bouton (-)
	private JButton btPoint; //Bouton .
	private JButton btEgal; //Bouton =
	private JButton btSupp; //Bouton pour effacer un caract�re
	private JButton btClear; //Boutton pour effacer tout le calcul
	
	//Variables :
	private double num1 = 0, num2 = 0, resultat = 0;
	private char opera; //L'opp�ration choisie
	
	//Constructeur :
	Calculatrice() {
		frame = new JFrame("Calculatrice - JAVA");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setSize(450, 550);
		frame.setLayout(null);
		
		textfield = new JTextField();
		textfield.setBounds(50, 25, 300, 50);
		textfield.setEditable(false);
		textfield.setBackground(Color.LIGHT_GRAY);
		
		//Construction des boutons op�ration :
		btPlus = new JButton("+");
		btSous = new JButton("-");
		btMult = new JButton("X");
		btDiv = new JButton("/");
		btNega = new JButton("(-)");
		btPoint = new JButton(".");
		btEgal = new JButton("=");
		btSupp = new JButton("Supp");
		btClear = new JButton("Clear");
		
		//Ajout des boutons d'op�rations dans le tableau des op�rations :
		operations[0] = btPlus;
		operations[1] = btSous;
		operations[2] =	btMult;
		operations[3] =	btDiv;
		operations[4] =	btNega;
		operations[5] =	btPoint;
		operations[6] =	btEgal;
		operations[7] =	btSupp;
		operations[8] =	btClear;
		
		//Rendre les boutons op�ration cliquable
		for(int i = 0; i < 9; i++) {
			operations[i].addActionListener(this);
			operations[i].setFocusable(false);
			operations[i].setBackground(Color.LIGHT_GRAY); //Couleurs des op�rations
		}
		operations[6].setBackground(Color.orange); //Couleur de l'op�ration "="
		
		//Instancier / Ajouter / Activer les bouton nombre
		for(int i = 0; i < 10; i++) {
			nombres[i] = new JButton(String.valueOf(i)); //Instanciation et Ajout des boutons nombre dans le tableau des nombres
			nombres[i].addActionListener(this); //Rend les boutons nombre cliquable
			nombres[i].setFocusable(false);
			nombres[i].setBackground(Color.gray); //Couleurs des chiffres
		}
		
		//Redimension de bouttons
		btNega.setBounds(50, 430, 100, 50);
		btSupp.setBounds(150, 430, 100, 50);
		btClear.setBounds(250, 430, 100, 50);
		
		//Panel : Ecran 
		panel = new JPanel();
		panel.setBounds(50, 100, 300, 300);
		panel.setLayout(new GridLayout(4,4,10,10));
		
		//Ajout des nombre dans le panel (Panel : Nombres) :
		panel.add(nombres[7]);
		panel.add(nombres[8]);
		panel.add(nombres[9]);
		panel.add(btMult);

		panel.add(nombres[4]);
		panel.add(nombres[5]);
		panel.add(nombres[6]);
		panel.add(btSous);
		
		panel.add(nombres[1]);
		panel.add(nombres[2]);
		panel.add(nombres[3]);
		panel.add(btPlus);
		
		panel.add(btPoint);
		panel.add(nombres[0]);
		panel.add(btEgal);
		panel.add(btDiv);
		
		
		frame.add(panel);
		frame.add(btNega);
		frame.add(btSupp);
		frame.add(btClear);
		frame.add(textfield);
		
		panel.setBackground(Color.DARK_GRAY); //Couleur de fond
		
		//Afficher la fenetre
		frame.setVisible(true); 
	}
	
	//Instanciation d'un �v�nement
	public void actionPerformed(ActionEvent e) {
		for(int i = 0; i < 10; i++) { //Parcours des nombres
			if(e.getSource() == nombres[i]) { //Si la touche (�v�nement) est un nombre
				textfield.setText(textfield.getText().concat(String.valueOf(i))); //On l'affiche sur l'ecran en le convertissant et en le concat�nant
			}
		}
		
		if(e.getSource() == btPoint) { //Si le bouton est un point
			textfield.setText(textfield.getText().concat(".")); //On affiche un point sur l'�cran
		}
		
		//Si l'�v�nement est l'op�ration +
		if(e.getSource() == btPlus) {
			num1 = Double.parseDouble(textfield.getText());
			opera = '+';
			textfield.setText("");
		}
		
		//Si l'�v�nement est l'op�ration -
		if(e.getSource() == btSous) {
			num1 = Double.parseDouble(textfield.getText());
			opera = '-';
			textfield.setText("");
		}
		
		//Si l'�v�nement est l'op�ration *
		if(e.getSource() == btMult) {
			num1 = Double.parseDouble(textfield.getText());
			opera = '*';
			textfield.setText("");
		}
		
		//Si l'�v�nement est l'op�ration /
		if(e.getSource() == btDiv) {
			num1 = Double.parseDouble(textfield.getText());
			opera = '/';
			textfield.setText("");
		}
		
		//Si l'�v�nement est l'op�ration 
		if(e.getSource() == btDiv) {
			num1 = Double.parseDouble(textfield.getText());
			opera = '/';
			textfield.setText("");
		}
		
		//Si l'�v�nement est l'op�ration =
		if(e.getSource() == btEgal) {
			num2 = Double.parseDouble(textfield.getText());
			
			switch(opera) {
			case '+':
				resultat = num1 + num2;
				break;
				
			case '-':
				resultat = num1 - num2;
				break;
				
			case '*':
				resultat = num1 * num2;
				break;
				
			case '/':
				resultat = num1 / num2;
				break;
			}
			
			textfield.setText(String.valueOf(resultat));
			num1 = resultat; //Stockage du resultat dans num 1 pour continuer des op�rations apr�s avoir afficher le resultat d'une op�ration
		}
		
		//Si on appuit sur le bouton clear tout s'efface sur l'�cran
		if(e.getSource() == btClear) {
			textfield.setText("");
		}
		
		//Si on appuit sur le bouton supprimer
		if(e.getSource() == btSupp) {
			String text = textfield.getText(); //Le nombre sur l'ecran est stock� dans la variable text
			textfield.setText(""); //L'�cran est r�nitialis� temporairement
			
			for(int i = 0; i < text.length()-1; i++) { //On parcours le nombre stock� avec le dernier caract�re en moins
				textfield.setText(textfield.getText() + text.charAt(i)); //On r��crit dans l'�cran le nombre stock� caract�re par caract�re avec la boucle (sans le dernier caract�re volontairement homis dnas la boucle)
			}
		}
		
		//Si on appuit sur le bouton n�gatif
		if(e.getSource() == btNega) {
			double tmp = Double.parseDouble(textfield.getText());
			tmp *= -1; //On multiplie la valeur stock� par -1 pour le rendre n�gatif
			textfield.setText(String.valueOf(tmp)); //convertion pour pouvoir l'afficher
		}
	}
	
}
